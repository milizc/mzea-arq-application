FROM node:16-alpine3.15
# Set the timezone in docker
RUN node --version
ENV TIME_ZONE=America/Argentina/Cordoba
RUN apk --update add tzdata && cp /usr/share/zoneinfo/America/Argentina/Cordoba /etc/localtime && echo 'America/Argentina/Cordoba' > /etc/timezone && apk del tzdata
WORKDIR /app
COPY db.json .
COPY package.json .
COPY *.js .
RUN ls -al
RUN npm install
EXPOSE 3000
ENTRYPOINT ["node", "index.js"]
